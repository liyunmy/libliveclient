#include "stdafx.h"
#include "../bin/HY_LiveClient.h"

#define SOK 0

int s_id1=0;
int s_id2=0;
int s_id3=0;
int s_id4=0;
int d_id1=0;


int LiveClientVStreamCallbackFun(int id ,BYTE* pBuf, int len, Uint32 ts, WORD seq, void *pUserData)
{
    printf("V id = %d len:%d\n",id,len);
	//if(id == s_id1)HY_DecoderPushData(d_id1,pBuf,len);	
	return SOK;
}


int LiveClientAStreamCallbackFun(int id ,BYTE* pBuf, int len, void *pUserData)
{
	//printf("A id = %d len:%d\n",id,len);
	return 0;
}

int LiveClientEventFun(int id ,int Envent, void *userpara)
{
	printf("id:%d Envent:%d\n",id,Envent);
	return SOK;
}

int main()
{
    //DWORD ver=0;
   // ver=HY_LiveServer_SDKVer();
    //printf("HY_LiveServer_SDKVer:0x%x\n",(Uint32)ver);
	HY_LiveClient_Init();

	s_id1 = HY_LiveClientCreateStream();
    //***********第一路***********
	RTSPParam mRTSPParam;
	mRTSPParam.useTCP=1;
	snprintf(mRTSPParam.IP,sizeof(mRTSPParam.IP),"%s","10.8.1.164");
	snprintf(mRTSPParam.UserName,sizeof(mRTSPParam.UserName),"%s","admin");
	snprintf(mRTSPParam.Password,sizeof(mRTSPParam.Password),"%s","hk123456");
	HY_RegLiveClientAStreamCallbackFun(s_id1,LiveClientAStreamCallbackFun);
	HY_RegLiveClientEventCallbackFun(s_id1,NULL,LiveClientEventFun);
	HY_RegLiveClientVStreamCallbackFun(s_id1,LiveClientVStreamCallbackFun);

	HY_LiveClientOpenStream(s_id1,(char*)"rtsp://10.8.1.164/h264/ch1/sub/av_stream",mRTSPParam);
	
	s_id2 = HY_LiveClientCreateStream();
    //***********第一路***********
	mRTSPParam.useTCP=1;
	snprintf(mRTSPParam.IP,sizeof(mRTSPParam.IP),"%s","10.8.1.164");
	snprintf(mRTSPParam.UserName,sizeof(mRTSPParam.UserName),"%s","admin");
	snprintf(mRTSPParam.Password,sizeof(mRTSPParam.Password),"%s","hk123456");
	HY_RegLiveClientAStreamCallbackFun(s_id2,LiveClientAStreamCallbackFun);
	HY_RegLiveClientEventCallbackFun(s_id2,NULL,LiveClientEventFun);
	HY_RegLiveClientVStreamCallbackFun(s_id2,LiveClientVStreamCallbackFun);

	HY_LiveClientOpenStream(s_id2,(char*)"rtsp://10.8.1.164/h264/ch1/main/av_stream",mRTSPParam);
	
	s_id3 = HY_LiveClientCreateStream();
    //***********第一路***********
	mRTSPParam.useTCP=1;
	snprintf(mRTSPParam.IP,sizeof(mRTSPParam.IP),"%s","10.11.5.166");
	snprintf(mRTSPParam.UserName,sizeof(mRTSPParam.UserName),"%s","admin");
	snprintf(mRTSPParam.Password,sizeof(mRTSPParam.Password),"%s","admin12345");
	HY_RegLiveClientAStreamCallbackFun(s_id3,LiveClientAStreamCallbackFun);
	HY_RegLiveClientEventCallbackFun(s_id3,NULL,LiveClientEventFun);
	HY_RegLiveClientVStreamCallbackFun(s_id3,LiveClientVStreamCallbackFun);

	HY_LiveClientOpenStream(s_id3,(char*)"rtsp://10.11.5.166/h264/ch1/main/av_stream",mRTSPParam);
	
	s_id4 = HY_LiveClientCreateStream();
    //***********第一路***********
	mRTSPParam.useTCP=1;
	snprintf(mRTSPParam.IP,sizeof(mRTSPParam.IP),"%s","10.11.5.166");
	snprintf(mRTSPParam.UserName,sizeof(mRTSPParam.UserName),"%s","admin");
	snprintf(mRTSPParam.Password,sizeof(mRTSPParam.Password),"%s","admin12345");
	HY_RegLiveClientAStreamCallbackFun(s_id4,LiveClientAStreamCallbackFun);
	HY_RegLiveClientEventCallbackFun(s_id4,NULL,LiveClientEventFun);
	HY_RegLiveClientVStreamCallbackFun(s_id4,LiveClientVStreamCallbackFun);

    HY_LiveClientOpenStream(s_id4,(char*)"rtsp://10.11.5.166/h264/ch1/main/av_stream",mRTSPParam);

	usleep(3*1000*1000);

	HY_LiveClientStopStream(s_id2);

	usleep(3*1000*1000);
	
	HY_LiveClientStopStream(s_id1);

	usleep(3*1000*1000);
	
	HY_LiveClientStopStream(s_id3);

	usleep(3*1000*1000);
	
	HY_LiveClientStopStream(s_id4);

    while(1)
    {
        usleep(100*1000);
    }
    return 0;
} 