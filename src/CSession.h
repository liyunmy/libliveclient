#ifndef _CSESSION_H_
#define _CSESSION_H_

#ifndef BYTE
#define BYTE unsigned char 
#endif
typedef BYTE *			LPBYTE;

//************事件标识**********
#define RTSP_EVE_STOPPED      0
#define RTSP_EVE_CONNECTING   1
#define RTSP_EVE_CONNFAIL     2
#define RTSP_EVE_CONNSUCC     3

#define RTSP_EVE_NOSIGNAL     4
#define RTSP_EVE_RESUME       5
#define RTSP_EVE_AUTHFAILED   6
#define RTSP_EVE_SERVERERROR  7

//**************回调************
typedef int (*notify_cb)(int ,int, void *);
typedef int (*video_cb)(int ,LPBYTE, int, unsigned int, unsigned short, void *);
typedef int (*audio_cb)(int ,LPBYTE, int, void *);

class CSession 
{
public:
 CSession();
 virtual ~CSession();

 RTSPClient* m_rtspClient;
 bool m_running;
 int m_nStatus;
 int m_nID;
 int is_useTCP;
 //string m_rtspUrl;
 //string m_progName;
 char str_rtspUrl[256];
 char str_progName[8];
 int m_debugLevel;

 void *m_pUserdata;
 //************相关回调***********
notify_cb       m_pNotify = NULL;
video_cb        m_pVideoCB = NULL;
audio_cb        m_pAudioCB = NULL;

};



#endif