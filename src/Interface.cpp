#include "stdafx.h"
#include "liveMedia.hh"
#include "BasicUsageEnvironment.hh"
#include "CSession.h"
#include "Rtsp.h"
#include "msgpri.h"

#define LIVE_EXPORTS
#ifdef LIVE_EXPORTS
#define HY_LIVE_API
#else
#define  HY_LIVE_API
#endif
#include "Interface.h"

#define SOK      0  ///< Status : OK
#define EFAIL   -1  ///< Status : Generic error


#define MAX_CHANNEL 8

CSession *pCSession[MAX_CHANNEL]={NULL};


/*
	函数	：	HY_LiveClient_SDKVer()
	参数	：	ver	-- 输出	--	的版本号
	返回值	：	0
	功能	：	获取版本
*/
HY_LIVE_API int HY_LiveClient_SDKVer(char *ver)
{
	sprintf(ver,"%s","V1.04.17914");
	return SOK ;
}

/*
	函数	：	HY_OnvifClient_Init()
	参数	：	无
	返回值	：	0	--	成功
				-1	--	失败
	功能	：	初始化并分配空间
*/
HY_LIVE_API int HY_LiveClient_Init()
{
	RtspInit();	
	return SOK;
}

HY_LIVE_API int HY_LiveClientCreateStream()
{
	int id=0;

	for (id=0;id<MAX_CHANNEL;id++)
	{
		if(pCSession[id] == NULL)
		{
			pCSession[id] = new CSession();
			pCSession[id]->m_nID=id;
			printf("初始化LiveClient模块:id=%d \n",id);
			return id;
		}
	}
	return EFAIL;
}

HY_LIVE_API int HY_LiveClientOpenStream(int id,char *URI,RTSPParam mRTSPParam)
{
    int ret=0;
	int type = 0;
	char URL_str[256]={0};
	char IP[16]={0};
	char UserName[32]={0};
	char Password[32]={0};
	char progName[16]={0};
	if (id < 0 || id >= MAX_CHANNEL)
	{
		return EFAIL;
	}
	if (pCSession[id] == NULL) return EFAIL;
	if (strlen(URI)<6) return EFAIL;
	if (strlen(mRTSPParam.IP)<7)return EFAIL;
	if(mRTSPParam.useTCP == 0 )
	{
		type = 0;
	}
	else
	{
		type = 1;
	}
	printf("************OpenStream-%d*************\n",id);
	/*
	sprintf(URL_str,"%s",URI);
	sprintf(IP,"%s",mRTSPParam.IP);
	sprintf(UserName,"%s",mRTSPParam.UserName);
	sprintf(Password,"%s",mRTSPParam.Password);
	*/
	memcpy(URL_str,URI,strlen(URI));
	if(URL_str[strlen(URI)-1] == 13)//去掉回车符
	{
		URL_str[strlen(URI)-1]=0;
	}
	memcpy(IP,mRTSPParam.IP,strlen(mRTSPParam.IP));
	if(IP[strlen(IP)-1] == 13)
	{
		//printf("****************URI*****************\n");
		IP[strlen(IP)-1]=0;
	}
	memcpy(UserName,mRTSPParam.UserName,strlen(mRTSPParam.UserName));
	if(UserName[strlen(UserName)-1] == 13)
	{
		//printf("****************UserName*****************\n");
		UserName[strlen(UserName)-1]=0;
	}
	memcpy(Password,mRTSPParam.Password,strlen(mRTSPParam.Password));
	if(Password[strlen(Password)-1] == 13)
	{
		//printf("****************Password*****************\n");
		Password[strlen(Password)-1]=0;
	}
	printf("LiveClient-id:%d UserName:%s Password:%s\n",id,UserName,Password);
	//******************把用户名密码加入URI******************
	//***查找是否有':'和'@'两个字符,有则不用再加入用户名密码
	int scount=0;
	for(int si=0;si<256;si++)
	{
		if (URL_str[si]=='@') {
				scount++;
		}
	}
	printf("LiveClient-scount:%d\n",scount);
	if(scount==0)
	{
		char h_str[8]="rtsp://";
		char *pstr=strstr(URL_str,h_str);
		if(pstr)
		{
			char tmpuri[256]={0};
			memcpy(tmpuri,URL_str+strlen(h_str),256-strlen(h_str));
			snprintf(URL_str,256,"%s%s:%s@%s",h_str,UserName,Password,tmpuri);
			printf("LiveClient-id:%d %s\n",id, URL_str);
		}
	}
	pCSession[id]->is_useTCP=type;
	//type=0;
	printf("####ch:%d URL:%s\n",id,URL_str);
	snprintf(progName,sizeof(progName),"%s","haoyun");
	//ret = pCSession[id]->startRTSPClient(progName,URL_str,1);
	startRTSPClient(&pCSession[id]->m_rtspClient,pCSession[id],progName,URL_str,1);
	/*
	printf("ret:%d\n",ret);
	printf("type : %d\n",type);
	printf("ch:%d IP :%s\n",id,mRTSPParam.IP);
	printf("ch:%d user :%s\n",id,mRTSPParam.UserName);
	printf("ch:%d passwd :%s\n",id,mRTSPParam.Password);
	printf("ch:%d URL:%s\n",id,URI);
	*/
	if(!ret)
	{
		return SOK;
	}
	else
	{
		return EFAIL;
	}
}

HY_LIVE_API int HY_LiveClientSetParamStream(int id,RTSPParam m_RTSPParam,char *url)
{
	/*
    int ret=0;
	if (id < 0 || id >= MAX_CHANNEL)
	{
		return EFAIL;
	}
	if (pCSession[id] == NULL) return EFAIL;
	ret=pCSession[id]->rtsp_setparam(m_RTSPParam,url);
	if(ret)
	{
		return SOK;
	}
	else
	{
		return EFAIL;
	}
	*/
	return SOK;
}

HY_LIVE_API int HY_LiveClientResetStream(int id,int type)
{
	/*
    int ret=0;
	if (id < 0 || id >= MAX_CHANNEL)
	{
		return EFAIL;
	}
	if (pCSession[id] == NULL) return EFAIL;
	int tmmtype=0;
	if(type)tmmtype=1;
	else tmmtype=0;
	ret=pCSession[id]->rtsp_reset(tmmtype);
	if(ret)
	{
		return SOK;
	}
	else
	{
		return EFAIL;
	}
	*/
	return SOK;
}


HY_LIVE_API int HY_LiveClientPlayStream(int id)
{
	/*
    int ret=0;
	if (id < 0 || id >= MAX_CHANNEL)
	{
		return EFAIL;
	}
	if (pCSession[id] == NULL) return EFAIL;
	ret=pCSession[id]->rtsp_play();
	if(ret)
	{
		return SOK;
	}
	else
	{
		return EFAIL;
	}
	*/
	return SOK;
}


HY_LIVE_API int HY_LiveClientStopStream(int id)
{
    int ret=0;
	if (id < 0 || id >= MAX_CHANNEL)
	{
		return EFAIL;
	}
	if (pCSession[id] == NULL) return EFAIL;
	//ret=pCSession[id]->stopRTSPClient();

	OS_EVENT("HY_LiveClientStopStream:%d\n",id);

	int tmp_nStatus = pCSession[id]->m_nStatus;
	
	if( (tmp_nStatus == RTSP_EVE_CONNECTING) || (tmp_nStatus == RTSP_EVE_CONNSUCC) || (tmp_nStatus == RTSP_EVE_RESUME))
    {
		shutdownStream(pCSession[id]->m_rtspClient,0);	
		//pCSession[id]->m_rtspClient=NULL;
    } 

	//pCSession[id]->m_rtspClient=NULL;

	if(ret==0)
	{
		return SOK;
	}
	else
	{
		return EFAIL;
	}
}


HY_LIVE_API int HY_LiveClientCloseStream(int id)
{
	/*
    int ret=0;
	if (id < 0 || id >= MAX_CHANNEL)
	{
		return EFAIL;
	}
	if (pCSession[id] == NULL) return EFAIL;
	ret=pCSession[id]->rtsp_close();
	if(ret)
	{
		return SOK;
	}
	else
	{
		return EFAIL;
	}
	*/
	return SOK;
}


HY_LIVE_API int HY_LiveClientReleaseStream(int id)
{
	if (id < 0 || id >= MAX_CHANNEL)
	{
		return EFAIL;
	}
	if (pCSession[id] == NULL) return EFAIL;

	if(pCSession[id] != NULL)
	{
		//pCSession[id]->stopRTSPClient();
		shutdownStream(pCSession[id]->m_rtspClient,1);		
		delete pCSession[id];
		pCSession[id] = NULL;
		return SOK;
	}
	return EFAIL;
}

//*******************设置数据回调*********************
HY_LIVE_API int  HY_RegLiveClientEventCallbackFun(int id,void *pUserData,int (__stdcall*fLiveClientEventFun)(int ,int, void *))
{
	if (id < 0 || id >= MAX_CHANNEL)
	{
		return EFAIL;
	}
	if (pCSession[id] == NULL) return EFAIL;
	pCSession[id]->m_pNotify = fLiveClientEventFun;
	pCSession[id]->m_pUserdata = pUserData;
	return SOK;
}

HY_LIVE_API int  HY_RegLiveClientVStreamCallbackFun(int id,int (__stdcall*fLiveClientVStreamFun)(int id ,BYTE* pBuf, int len, Uint32 ts, WORD seq, void *pUserData))
{
	if (id < 0 || id >= MAX_CHANNEL)
	{
		return EFAIL;
	}
	if (pCSession[id] == NULL) return EFAIL;

	pCSession[id]->	m_pVideoCB = fLiveClientVStreamFun;
	//pCSession[id]->m_pUserdata = pUserData;
	return SOK;
}

HY_LIVE_API int  HY_RegLiveClientAStreamCallbackFun(int id,int (__stdcall*fLiveClientAStreamFun)(int id ,BYTE* pBuf, int len, void *pUserData))
{
	if (id < 0 || id >= MAX_CHANNEL)
	{
		return EFAIL;
	}
	if (pCSession[id] == NULL) return EFAIL;

	pCSession[id]->m_pAudioCB = fLiveClientAStreamFun;
	//pCSession[id]->m_pUserdata = pUserData;
	return SOK;
}
