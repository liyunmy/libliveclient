#ifndef _STDAFX_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define _STDAFX_H_

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdarg.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <ctype.h>
#include <pthread.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/ioctl.h>                 
#include <arpa/inet.h>       
#include <getopt.h>
#include <error.h>

#include  <unistd.h>     /*Unix 标准函数定义*/
#include  <sys/types.h>  
#include  <sys/stat.h>   
#include  <fcntl.h>      /*文件控制定义*/
#include  <termios.h>    /*PPSIX 终端控制定义*/
#include   <string.h>
#include  <errno.h>      /*错误号定义*/


#include <sys/vfs.h>//计算SD卡容量
#include <sys/mount.h>//挂载SD卡

#include <sys/ipc.h>//共享内存
#include <sys/shm.h>

#ifndef BYTE
typedef unsigned char BYTE;            ///< Unsigned  char
#endif

#ifndef Uint8
typedef unsigned char Uint8;            ///< Unsigned  8-bit integer
#endif

#ifndef Uint16
typedef unsigned short Uint16;          ///< Unsigned 16-bit integer
#endif

#ifndef Uint32
typedef unsigned int Uint32;            ///< Unsigned 32-bit integer
#endif

#ifndef WORD
typedef unsigned short WORD;
#endif

#ifndef DWORD
typedef unsigned long DWORD;
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif
