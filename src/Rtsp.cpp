#include "stdafx.h"
#include "liveMedia.hh"
#include "BasicUsageEnvironment.hh"
#include "CSession.h"
#include "Rtsp.h"
#include "msgpri.h"

static unsigned rtspClientCount = 0; // Counts how many streams (i.e., "RTSPClient"s) are currently in use.

TaskScheduler* scheduler = NULL;
UsageEnvironment* env =NULL;
char eventLoopWatchVariable=0;
pthread_t tid;

//******************一些功能函数******************
// Used to shut down and close a stream (including its "RTSPClient" object):

void *rtsp_thread_fun(void *param);
void shutdownStream(RTSPClient* rtspClient, int exitCode = 1);
void continueAfterDESCRIBE(RTSPClient* rtspClient, int resultCode, char* resultString);
void setupNextSubsession(RTSPClient* rtspClient);

void continueAfterSETUP(RTSPClient* rtspClient, int resultCode, char* resultString);
void continueAfterPLAY(RTSPClient* rtspClient, int resultCode, char* resultString);

void subsessionAfterPlaying(void* clientData);
void subsessionByeHandler(void* clientData);
void streamTimerHandler(void* clientData);


int RtspInit()
{
    printf("**********RtspInit**********\n");
    eventLoopWatchVariable = 0;
    scheduler = BasicTaskScheduler::createNew();
    env = BasicUsageEnvironment::createNew(*scheduler);

    int r = pthread_create(&tid, NULL, rtsp_thread_fun, NULL);
    if (r)
    {
        perror ("pthread_create()");
        return -1;
    }
    return 0;
}

int startRTSPClient( RTSPClient** m_rtspClient,CSession *p_CSession,char const* progName, char const* rtspURL, int debugLevel)
{
    *m_rtspClient = ourRTSPClient::createNew(*env, rtspURL, debugLevel, progName);
    if (*m_rtspClient == NULL) 
    {
        *env << "Failed to create a RTSP client for URL \"" << rtspURL << "\": " << env->getResultMsg() << "\n";
        return -1;
    }
    rtspClientCount++;
    ((ourRTSPClient*)*m_rtspClient)->m_nID = p_CSession->m_nID;
    ((ourRTSPClient*)*m_rtspClient)->p_nStatus = &p_CSession->m_nStatus;
    ((ourRTSPClient*)*m_rtspClient)->is_useTCP   = p_CSession->is_useTCP;
    ((ourRTSPClient*)*m_rtspClient)->m_pUserdata = p_CSession->m_pUserdata;
    ((ourRTSPClient*)*m_rtspClient)->m_pNotify   = p_CSession->m_pNotify;
    ((ourRTSPClient*)*m_rtspClient)->m_pVideoCB  = p_CSession->m_pVideoCB;
    ((ourRTSPClient*)*m_rtspClient)->m_pAudioCB  = p_CSession->m_pAudioCB;
    ((ourRTSPClient*)*m_rtspClient)->eventLoopWatchVariable=&eventLoopWatchVariable;
    p_CSession->m_pNotify( p_CSession->m_nID,RTSP_EVE_CONNECTING, p_CSession->m_pUserdata );
    //printf("**************start continueAfterDESCRIBE !\n");
    (*m_rtspClient)->sendDescribeCommand(continueAfterDESCRIBE); 
    //printf("continueAfterDESCRIBE completed !\n");
    return 0;
}

void *rtsp_thread_fun(void *param)
{
 //::startRTSP(m_progName.c_str(), m_rtspUrl.c_str(), m_ndebugLever);
    //TaskScheduler* scheduler = BasicTaskScheduler::createNew();
    //UsageEnvironment* env = BasicUsageEnvironment::createNew(*scheduler);
  
    //m_nStatus = RTSP_EVE_STOPPED;
    while(1)
    {
    //printf("rtsp_thread_fun!\n");
    env->taskScheduler().doEventLoop(&eventLoopWatchVariable);//不断执行事件,时钟作用
    eventLoopWatchVariable = 0;
    APP_ALARM("eventLoopWatchVariable!\n");
    /*
    if (m_rtspClient)
    {
        printf("rtsp_fun shutdownStream!\n");
        if(RTSP_EVE_STOPPED!=RTSP_EVE_STOPPED)
        {
            shutdownStream(m_rtspClient,0);
        }
        printf("rtsp_fun shutdownStream-eee!\n");
    }
    m_rtspClient = NULL;
    */
    //usleep(100*1000);
    }

    env->reclaim(); 
    env = NULL;
    delete scheduler; 
    scheduler = NULL;
    return NULL;
}

// A function that outputs a string that identifies each stream (for debugging output). Modify this if you wish:
UsageEnvironment& operator<<(UsageEnvironment& env, const RTSPClient& rtspClient) 
{
    return env << "[URL:\"" << rtspClient.url() << "\"]: ";
}
// A function that outputs a string that identifies each subsession (for debugging output). Modify this if you wish:
UsageEnvironment& operator<<(UsageEnvironment& env, const MediaSubsession& subsession) 
{
    return env << subsession.mediumName() << "/" << subsession.codecName();
}
void usage(UsageEnvironment& env, char const* progName) 
{
    env << "Usage: " << progName << " <rtsp-url-1> ... <rtsp-url-N>\n";
    env << "\t(where each <rtsp-url-i> is a \"rtsp://\" URL)\n";
}

//**************Implementation of "ourRTSPClient"********************

ourRTSPClient* ourRTSPClient::createNew(UsageEnvironment& env, char const* rtspURL,
					int verbosityLevel, char const* applicationName, portNumBits tunnelOverHTTPPortNum) {
  return new ourRTSPClient(env, rtspURL, verbosityLevel, applicationName, tunnelOverHTTPPortNum);
}

ourRTSPClient::ourRTSPClient(UsageEnvironment& env, char const* rtspURL,
			     int verbosityLevel, char const* applicationName, portNumBits tunnelOverHTTPPortNum)
  : RTSPClient(env,rtspURL, verbosityLevel, applicationName, tunnelOverHTTPPortNum, -1) {
    OS_EVENT("************id:%d ourRTSPClient***********\n",m_nID);
}

ourRTSPClient::~ourRTSPClient() {
    OS_EVENT("************id:%d ~ourRTSPClient***********\n",m_nID);
}

//****************Implementation of "StreamClientState"**********************

StreamClientState::StreamClientState()
  : iter(NULL), session(NULL), subsession(NULL), streamTimerTask(NULL), duration(0.0) {
}

StreamClientState::~StreamClientState() {
  delete iter;
  if (session != NULL) {
    // We also need to delete "session", and unschedule "streamTimerTask" (if set)
    UsageEnvironment& env = session->envir(); // alias

    env.taskScheduler().unscheduleDelayedTask(streamTimerTask);
    Medium::close(session);
  }
}

// Implementation of "DummySink":

// Even though we're not going to be doing anything with the incoming data, we still need to receive it.
// Define the size of the buffer that we'll use:
#define DUMMY_SINK_RECEIVE_BUFFER_SIZE 2000000

DummySink* DummySink::createNew(UsageEnvironment& env, MediaSubsession& subsession, char const* streamId) {
  return new DummySink(env, subsession, streamId);
}

DummySink::DummySink(UsageEnvironment& env, MediaSubsession& subsession, char const* streamId)
  : MediaSink(env),
    fSubsession(subsession) {
  fStreamId = strDup(streamId);
  fReceiveBuffer = new u_int8_t[DUMMY_SINK_RECEIVE_BUFFER_SIZE];
  pvdata = new u_int8_t[DUMMY_SINK_RECEIVE_BUFFER_SIZE+4] ;
  padata = new u_int8_t[DUMMY_SINK_RECEIVE_BUFFER_SIZE+4];
  m_nID=-1;  
  firstFrame=1;
  printf("..................DummySink\n");
}

DummySink::~DummySink() {
  delete[] fReceiveBuffer;
  delete[] fStreamId;
  delete[] pvdata;
  delete[] padata;
  printf("..................~DummySink\n");
}

void DummySink::afterGettingFrame(void* clientData, unsigned frameSize, unsigned numTruncatedBytes,
				  struct timeval presentationTime, unsigned durationInMicroseconds) {
  DummySink* sink = (DummySink*)clientData;
  //printf("DummySink name :%s\n",sink->fSubsession.mediumName());
  //usage(sink->envir(),"progName1");
  sink->afterGettingFrame(frameSize, numTruncatedBytes, presentationTime, durationInMicroseconds);
}

// If you don't want to see debugging output for each received frame, then comment out the following line:
//#define DEBUG_PRINT_EACH_RECEIVED_FRAME 0
void DummySink::afterGettingFrame(unsigned frameSize, unsigned numTruncatedBytes,
				  struct timeval presentationTime, unsigned /*durationInMicroseconds*/) {
  // We've just received a frame of data.  (Optionally) print out information about it:
#ifdef DEBUG_PRINT_EACH_RECEIVED_FRAME
  if (fStreamId != NULL) envir() << "Stream \"" << fStreamId << "\"; ";
  envir() << fSubsession.mediumName() << "/" << fSubsession.codecName() << ":\tReceived " << frameSize << " bytes";
  if (numTruncatedBytes > 0) envir() << " (with " << numTruncatedBytes << " bytes truncated)";
  char uSecsStr[6+1]; // used to output the 'microseconds' part of the presentation time
  sprintf(uSecsStr, "%06u", (unsigned)presentationTime.tv_usec);
  envir() << ".\tPresentation time: " << (int)presentationTime.tv_sec << "." << uSecsStr;
  if (fSubsession.rtpSource() != NULL && !fSubsession.rtpSource()->hasBeenSynchronizedUsingRTCP()) {
    envir() << "!"; // mark the debugging output to indicate that this presentation time is not RTCP-synchronized
  }
#ifdef DEBUG_PRINT_NPT
  envir() << "\tNPT: " << fSubsession.getNormalPlayTime(presentationTime);
#endif
  envir() << "\n";
#endif
//printf("###m_nID:%d\n",m_nID);
//printf("###%s\n",fStreamId);
   if(!strcmp(fSubsession.mediumName(), "video"))
  {

    if(firstFrame)
    {
        unsigned int num;
        SPropRecord *sps = parseSPropParameterSets(fSubsession.fmtp_spropparametersets(), num);
        // For H.264 video stream, we use a special sink that insert start_codes:
       // struct timeval tv= {0,0};
        unsigned char start_code[4] = {0x00, 0x00, 0x00, 0x01};
        /*
        FILE *fp = fopen("test.264", "a+b");
        if(fp)
        {
            fwrite(start_code, 4, 1, fp);
            fwrite(sps[0].sPropBytes, sps[0].sPropLength, 1, fp);
            fwrite(start_code, 4, 1, fp);
            fwrite(sps[1].sPropBytes, sps[1].sPropLength, 1, fp);
            fclose(fp);
            fp = NULL;
        }
        */
      #if 0
        BYTE *pvdata = (BYTE*)malloc(sps[0].sPropLength+4);
        memcpy(pvdata,start_code,4);
        memcpy(pvdata+4,sps[0].sPropBytes,sps[0].sPropLength);
        if (m_pVideoCB)
        {
           m_pVideoCB(m_nID,pvdata, sps[0].sPropLength+4, (int)presentationTime.tv_sec, 25, m_pUserdata);
        }
        usleep(20000);
        free(pvdata);

        pvdata = (BYTE*)malloc(sps[1].sPropLength+4);
        memcpy(pvdata,start_code,4);
        memcpy(pvdata+4,sps[1].sPropBytes,sps[1].sPropLength);
        if (m_pVideoCB)
        {
           m_pVideoCB(m_nID,pvdata, sps[1].sPropLength+4, (int)presentationTime.tv_sec, 25, m_pUserdata);
        }
        usleep(20000);
        free(pvdata);
      #else
        memcpy(pvdata,start_code,4);
        memcpy(pvdata+4,sps[0].sPropBytes,sps[0].sPropLength);
        memcpy(pvdata+4+sps[0].sPropLength,start_code,4);
        memcpy(pvdata+4+sps[0].sPropLength+4,sps[1].sPropBytes,sps[1].sPropLength);

        if (m_pVideoCB)
        {
           m_pVideoCB(m_nID,pvdata, sps[0].sPropLength+4+sps[1].sPropLength+4, (int)presentationTime.tv_sec, 25, m_pUserdata);
        }
        usleep(20000);
      #endif

        delete [] sps;
        firstFrame = False;
    }

     char head[4] = {0x00, 0x00, 0x00, 0x01};
     memset(pvdata,0,frameSize+4);
     memcpy(pvdata,head,4);
     memcpy(pvdata+4,(BYTE*)fReceiveBuffer,frameSize);
     if (m_pVideoCB)
     {
        m_pVideoCB(m_nID,pvdata, frameSize+4, (int)presentationTime.tv_sec, 25, m_pUserdata);
     }
  }

  if(!strcmp(fSubsession.mediumName(), "audio"))
  {
     char head[4] = {0x00, 0x00, 0x00, 0x01};
     memset(pvdata,0,frameSize+4);
     memcpy(pvdata,head,4);
     memcpy(pvdata+4,(BYTE*)fReceiveBuffer,frameSize);
    if (m_pAudioCB)
    {
        m_pAudioCB(m_nID,fReceiveBuffer, frameSize-12, m_pUserdata);
       // printf("audio.......................\n");
    }
  }
  // Then continue, to request the next frame of data:
  continuePlaying();
}

Boolean DummySink::continuePlaying() {
  if (fSource == NULL) return False; // sanity check (should not happen)

  //printf(".....................DummySink::continuePlaying!\n");

  // Request the next frame of data from our input source.  "afterGettingFrame()" will get called later, when it arrives:
  fSource->getNextFrame(fReceiveBuffer, DUMMY_SINK_RECEIVE_BUFFER_SIZE,
                        afterGettingFrame, this,
                        onSourceClosure, this);
  return True;
}


//******************一些功能函数******************
void shutdownStream(RTSPClient* rtspClient, int exitCode) {
  int tmp_nStatus = ((ourRTSPClient*)rtspClient)->p_nStatus[0];
/*
    if( (tmp_nStatus == RTSP_EVE_CONNECTING) || (tmp_nStatus == RTSP_EVE_CONNSUCC) || (tmp_nStatus == RTSP_EVE_RESUME))
    {
        printf("enter shutdownStream!\n");        
    } 
    else
    {
        printf("rtspClient:0x%x\n",rtspClient);
        printf("id:%d 不是连接状态，不用闭关码流!\n",((ourRTSPClient*)rtspClient)->m_nID);
        return ;
    } 
*/
  UsageEnvironment& env = rtspClient->envir(); // alias
  StreamClientState& scs = ((ourRTSPClient*)rtspClient)->scs; // alias
  printf("shutdownStream:First\n");
  // First, check whether any subsessions have still to be closed:
  if (scs.session != NULL) { 
    Boolean someSubsessionsWereActive = False;
    MediaSubsessionIterator iter(*scs.session);
    MediaSubsession* subsession;

    while ((subsession = iter.next()) != NULL) {
      if (subsession->sink != NULL) {
	Medium::close(subsession->sink);
	subsession->sink = NULL;

	if (subsession->rtcpInstance() != NULL) {
	  subsession->rtcpInstance()->setByeHandler(NULL, NULL); // in case the server sends a RTCP "BYE" while handling "TEARDOWN"
	}

	someSubsessionsWereActive = True;
      }
    }
    printf("shutdownStream TEARDOWN!\n");
    if (someSubsessionsWereActive) {
      // Send a RTSP "TEARDOWN" command, to tell the server to shutdown the stream.
      // Don't bother handling the response to the "TEARDOWN".
      rtspClient->sendTeardownCommand(*scs.session, NULL);
    }
  }

  env << *rtspClient << "Closing the stream.\n";
  Medium::close(rtspClient);
  //************关闭码流*************
  printf("关闭码流!\n");

  if( (tmp_nStatus == RTSP_EVE_CONNECTING) || (tmp_nStatus == RTSP_EVE_CONNSUCC) || (tmp_nStatus == RTSP_EVE_RESUME))
  {
    ((ourRTSPClient*)rtspClient)->m_pNotify( ((ourRTSPClient*)rtspClient)->m_nID,RTSP_EVE_STOPPED, ((ourRTSPClient*)rtspClient)->m_pUserdata );
  }

 ((ourRTSPClient*)rtspClient)->p_nStatus[0] = RTSP_EVE_STOPPED;
    // Note that this will also cause this stream's "StreamClientState" structure to get reclaimed.

  if (--rtspClientCount == 0) {
    OS_EVENT("shutdownStream---enter exit!\n");
    // The final stream has ended, so exit the application now.
    // (Of course, if you're embedding this code into your own application, you might want to comment this out,
    // and replace it with "eventLoopWatchVariable = 1;", so that we leave the LIVE555 event loop, and continue running "main()".)
    //exit(exitCode);
  }
  printf("leave shutdownStream!\n");
}
// Implementation of the RTSP 'response handlers':

void continueAfterDESCRIBE(RTSPClient* rtspClient, int resultCode, char* resultString) 
{
  do {
    UsageEnvironment& env = rtspClient->envir(); // alias
    StreamClientState& scs = ((ourRTSPClient*)rtspClient)->scs; // alias

    if (resultCode != 0) {
      env << *rtspClient << "Failed to get a SDP description: " << resultString << "\n";
      printf("resultCode:%d\n",resultCode);
      //*************设备不在线**************
      if(resultCode == -115)
      {
        ((ourRTSPClient*)rtspClient)->p_nStatus[0] = RTSP_EVE_STOPPED;
        ((ourRTSPClient*)rtspClient)->m_pNotify( ((ourRTSPClient*)rtspClient)->m_nID,RTSP_EVE_CONNFAIL, ((ourRTSPClient*)rtspClient)->m_pUserdata );
      }
      if(resultCode == 401)
      {
          char h_str[]="Unauthorized";
          char *pstr=strstr(resultString,h_str);
          if(pstr)
          {
             ((ourRTSPClient*)rtspClient)->p_nStatus[0] = RTSP_EVE_AUTHFAILED;
             ((ourRTSPClient*)rtspClient)->m_pNotify( ((ourRTSPClient*)rtspClient)->m_nID,RTSP_EVE_AUTHFAILED, ((ourRTSPClient*)rtspClient)->m_pUserdata );
          }
      }
      delete[] resultString;
      break;
    }

    char* const sdpDescription = resultString;
    env << *rtspClient << "Got a SDP description:\n" << sdpDescription << "\n";

    // Create a media session object from this SDP description:
    scs.session = MediaSession::createNew(env, sdpDescription);
    delete[] sdpDescription; // because we don't need it anymore
    if (scs.session == NULL) {
      env << *rtspClient << "Failed to create a MediaSession object from the SDP description: " << env.getResultMsg() << "\n";
      break;
    } else if (!scs.session->hasSubsessions()) {
      env << *rtspClient << "This session has no media subsessions (i.e., no \"m=\" lines)\n";
      break;
    }

    // Then, create and set up our data source objects for the session.  We do this by iterating over the session's 'subsessions',
    // calling "MediaSubsession::initiate()", and then sending a RTSP "SETUP" command, on each one.
    // (Each 'subsession' will have its own data source.)
    scs.iter = new MediaSubsessionIterator(*scs.session);
    setupNextSubsession(rtspClient);
    return;
  } while (0);

  // An unrecoverable error occurred with this stream.
  shutdownStream(rtspClient);
}

void setupNextSubsession(RTSPClient* rtspClient) {
  UsageEnvironment& env = rtspClient->envir(); // alias
  StreamClientState& scs = ((ourRTSPClient*)rtspClient)->scs; // alias

  printf("setupNextSubsession!\n");
  
  scs.subsession = scs.iter->next();
  if (scs.subsession != NULL) {
    if (!scs.subsession->initiate()) {
      env << *rtspClient << "Failed to initiate the \"" << *scs.subsession << "\" subsession: " << env.getResultMsg() << "\n";
      setupNextSubsession(rtspClient); // give up on this subsession; go to the next one
    } else {
      env << *rtspClient << "Initiated the \"" << *scs.subsession << "\" subsession (";
      if (scs.subsession->rtcpIsMuxed()) {
	env << "client port " << scs.subsession->clientPortNum();
      } else {
	env << "client ports " << scs.subsession->clientPortNum() << "-" << scs.subsession->clientPortNum()+1;
      }
      env << ")\n";

      //Continue setting up this subsession, by sending a RTSP "SETUP" command:
      //rtspClient->sendSetupCommand(*scs.subsession, continueAfterSETUP, False, REQUEST_STREAMING_OVER_TCP);
      rtspClient->sendSetupCommand(*scs.subsession, continueAfterSETUP, False,  ((ourRTSPClient*)rtspClient)->is_useTCP);
    }
    return;
  }

  // We've finished setting up all of the subsessions.  Now, send a RTSP "PLAY" command to start the streaming:
  if (scs.session->absStartTime() != NULL) {
    // Special case: The stream is indexed by 'absolute' time, so send an appropriate "PLAY" command:
    rtspClient->sendPlayCommand(*scs.session, continueAfterPLAY, scs.session->absStartTime(), scs.session->absEndTime());
  } else {
    scs.duration = scs.session->playEndTime() - scs.session->playStartTime();
    rtspClient->sendPlayCommand(*scs.session, continueAfterPLAY);
  }
}



void continueAfterSETUP(RTSPClient* rtspClient, int resultCode, char* resultString) {
  do {
    UsageEnvironment& env = rtspClient->envir(); // alias
    StreamClientState& scs = ((ourRTSPClient*)rtspClient)->scs; // alias

    if (resultCode != 0) {
      env << *rtspClient << "Failed to set up the \"" << *scs.subsession << "\" subsession: " << resultString << "\n";

      //if(resultCode == 500)
      {
          char h_str[]="Internal Server Error";
          char *pstr=strstr(resultString,h_str);
          if(pstr)
          {
                ((ourRTSPClient*)rtspClient)->p_nStatus[0] = RTSP_EVE_SERVERERROR;
                ((ourRTSPClient*)rtspClient)->m_pNotify( ((ourRTSPClient*)rtspClient)->m_nID,RTSP_EVE_SERVERERROR, ((ourRTSPClient*)rtspClient)->m_pUserdata );
          }
      }

      break;
    }

    env << *rtspClient << "Set up the \"" << *scs.subsession << "\" subsession (";
    if (scs.subsession->rtcpIsMuxed()) {
      env << "client port " << scs.subsession->clientPortNum();
    } else {
      env << "client ports " << scs.subsession->clientPortNum() << "-" << scs.subsession->clientPortNum()+1;
    }
    env << ")\n";
    // Having successfully setup the subsession, create a data sink for it, and call "startPlaying()" on it.
    // (This will prepare the data sink to receive data; the actual flow of data from the client won't start happening until later,
    // after we've sent a RTSP "PLAY" command.)

    //流媒体数据接收的一些准备工作
    scs.subsession->sink = DummySink::createNew(env, *scs.subsession, rtspClient->url());
      // perhaps use your own custom "MediaSink" subclass instead
    if (scs.subsession->sink == NULL) {
      env << *rtspClient << "Failed to create a data sink for the \"" << *scs.subsession
	  << "\" subsession: " << env.getResultMsg() << "\n";
      break;
    }
    ((DummySink*)scs.subsession->sink)->m_nID = ((ourRTSPClient*)rtspClient)->m_nID;
    ((DummySink*)scs.subsession->sink)->m_pUserdata = ((ourRTSPClient*)rtspClient)->m_pUserdata;
    ((DummySink*)scs.subsession->sink)->m_pNotify   = ((ourRTSPClient*)rtspClient)->m_pNotify;
    ((DummySink*)scs.subsession->sink)->m_pVideoCB  = ((ourRTSPClient*)rtspClient)->m_pVideoCB;
    ((DummySink*)scs.subsession->sink)->m_pAudioCB  = ((ourRTSPClient*)rtspClient)->m_pAudioCB;
    ((DummySink*)scs.subsession->sink)->eventLoopWatchVariable=((ourRTSPClient*)rtspClient)->eventLoopWatchVariable;
    env << *rtspClient << "Created a data sink for the \"" << *scs.subsession << "\" subsession\n";
    scs.subsession->miscPtr = rtspClient; // a hack to let subsession handler functions get the "RTSPClient" from the subsession 
    scs.subsession->sink->startPlaying(*(scs.subsession->readSource()),
				       subsessionAfterPlaying, scs.subsession);
    // Also set a handler to be called if a RTCP "BYE" arrives for this subsession:
    if (scs.subsession->rtcpInstance() != NULL) {
      scs.subsession->rtcpInstance()->setByeHandler(subsessionByeHandler, scs.subsession);
    }
  } while (0);
  delete[] resultString;

  // Set up the next subsession, if any:
  setupNextSubsession(rtspClient);
}

void continueAfterPLAY(RTSPClient* rtspClient, int resultCode, char* resultString) {
  Boolean success = False;

  do {
    UsageEnvironment& env = rtspClient->envir(); // alias
    StreamClientState& scs = ((ourRTSPClient*)rtspClient)->scs; // alias

    if (resultCode != 0) {
      env << *rtspClient << "Failed to start playing session: " << resultString << "\n";
      break;
    }

    // Set a timer to be handled at the end of the stream's expected duration (if the stream does not already signal its end
    // using a RTCP "BYE").  This is optional.  If, instead, you want to keep the stream active - e.g., so you can later
    // 'seek' back within it and do another RTSP "PLAY" - then you can omit this code.
    // (Alternatively, if you don't want to receive the entire stream, you could set this timer for some shorter value.)
    if (scs.duration > 0) {
      unsigned const delaySlop = 2; // number of seconds extra to delay, after the stream's expected duration.  (This is optional.)
      scs.duration += delaySlop;
      unsigned uSecsToDelay = (unsigned)(scs.duration*1000000);
      scs.streamTimerTask = env.taskScheduler().scheduleDelayedTask(uSecsToDelay, (TaskFunc*)streamTimerHandler, rtspClient);
    }

    env << *rtspClient << "Started playing session";
    if (scs.duration > 0) {
      env << " (for up to " << scs.duration << " seconds)";
    }
    env << "...\n";

    success = True;
    ((ourRTSPClient*)rtspClient)->p_nStatus[0] = RTSP_EVE_CONNSUCC;
    //*****************连接成功**************
    ((ourRTSPClient*)rtspClient)->m_pNotify( ((ourRTSPClient*)rtspClient)->m_nID,RTSP_EVE_CONNSUCC, ((ourRTSPClient*)rtspClient)->m_pUserdata );

  } while (0);
  delete[] resultString;

  if (!success) {
    // An unrecoverable error occurred with this stream.
    shutdownStream(rtspClient);
  }
}

// Implementation of the other event handlers:

void subsessionAfterPlaying(void* clientData) {
  MediaSubsession* subsession = (MediaSubsession*)clientData;
  RTSPClient* rtspClient = (RTSPClient*)(subsession->miscPtr);

  // Begin by closing this subsession's stream:
  Medium::close(subsession->sink);
  subsession->sink = NULL;

  // Next, check whether *all* subsessions' streams have now been closed:
  MediaSession& session = subsession->parentSession();
  MediaSubsessionIterator iter(session);
  while ((subsession = iter.next()) != NULL) {
    if (subsession->sink != NULL) return; // this subsession is still active
  }

  // All subsessions' streams have now been closed, so shutdown the client:
  shutdownStream(rtspClient);
}

void subsessionByeHandler(void* clientData) {
  MediaSubsession* subsession = (MediaSubsession*)clientData;
  RTSPClient* rtspClient = (RTSPClient*)subsession->miscPtr;
  UsageEnvironment& env = rtspClient->envir(); // alias

  env << *rtspClient << "Received RTCP \"BYE\" on \"" << *subsession << "\" subsession\n";

  // Now act as if the subsession had closed:
  subsessionAfterPlaying(subsession);
}

void streamTimerHandler(void* clientData) {
  ourRTSPClient* rtspClient = (ourRTSPClient*)clientData;
  StreamClientState& scs = rtspClient->scs; // alias

  scs.streamTimerTask = NULL;

  // Shut down the stream:
  shutdownStream(rtspClient);
}