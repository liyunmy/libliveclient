#ifndef _RTSP_
#define _RTSP_

// Define a class to hold per-stream state that we maintain throughout each stream's lifetime:

class StreamClientState {
public:
  StreamClientState();
  virtual ~StreamClientState();

public:
  MediaSubsessionIterator* iter;
  MediaSession* session;
  MediaSubsession* subsession;
  TaskToken streamTimerTask;
  double duration;
};


//过程实现
//***********ourRTSPClient类***********
class ourRTSPClient: public RTSPClient {
public:
  static ourRTSPClient* createNew(UsageEnvironment& env, char const* rtspURL,
				  int verbosityLevel = 0,
				  char const* applicationName = NULL,
				  portNumBits tunnelOverHTTPPortNum = 0);

protected:
  ourRTSPClient(UsageEnvironment& env, char const* rtspURL,
		int verbosityLevel, char const* applicationName, portNumBits tunnelOverHTTPPortNum);
    // called only by createNew();
  virtual ~ourRTSPClient();

public:
    int m_nID;//类ID
    int is_useTCP;//是否使用TCP协议
    int *p_nStatus;
    char *eventLoopWatchVariable;
    StreamClientState scs;
    void *          m_pUserdata = NULL;
    notify_cb       m_pNotify = NULL;
    video_cb        m_pVideoCB = NULL;
    audio_cb        m_pAudioCB = NULL;
};

// Define a data sink (a subclass of "MediaSink") to receive the data for each subsession (i.e., each audio or video 'substream').
// In practice, this might be a class (or a chain of classes) that decodes and then renders the incoming audio or video.
// Or it might be a "FileSink", for outputting the received data into a file (as is done by the "openRTSP" application).
// In this example code, however, we define a simple 'dummy' sink that receives incoming data, but does nothing with it.

//数据接收
class DummySink: public MediaSink {
public:
  static DummySink* createNew(UsageEnvironment& env,
			      MediaSubsession& subsession, // identifies the kind of data that's being received
			      char const* streamId = NULL); // identifies the stream itself (optional)

private:
  DummySink(UsageEnvironment& env, MediaSubsession& subsession, char const* streamId);
    // called only by "createNew()"
  virtual ~DummySink();

  static void afterGettingFrame(void* clientData, unsigned frameSize,
                                unsigned numTruncatedBytes,
				struct timeval presentationTime,
                                unsigned durationInMicroseconds);
  void afterGettingFrame(unsigned frameSize, unsigned numTruncatedBytes,
			 struct timeval presentationTime, unsigned durationInMicroseconds);

private:
  // redefined virtual functions:
  virtual Boolean continuePlaying();

private:
    int firstFrame;
    u_int8_t* fReceiveBuffer;
    MediaSubsession& fSubsession;
    char* fStreamId;
public:
     int m_nID;
     u_int8_t* pvdata;
     u_int8_t* padata;
     char *eventLoopWatchVariable;
     void *          m_pUserdata = NULL;
     notify_cb       m_pNotify = NULL;
     video_cb        m_pVideoCB = NULL;
     audio_cb        m_pAudioCB = NULL;
};


//******************接口*********************
int RtspInit();
int startRTSPClient( RTSPClient** m_rtspClient,CSession *p_CSession,char const* progName, char const* rtspURL, int debugLevel);
void shutdownStream(RTSPClient* rtspClient, int exitCode );

#endif