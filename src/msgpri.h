#ifndef _MSGPRI_H_

#define _MSGPRI_H_
//信息打印
//**************************************printf打印颜色*************************************
#define NONE          "\033[m"   
#define RED           "\033[0;32;31m"   
#define LIGHT_RED     "\033[1;31m"   
#define GREEN         "\033[0;32;32m"   
#define LIGHT_GREEN   "\033[1;32m"   
#define BLUE          "\033[0;32;34m"   
#define LIGHT_BLUE    "\033[1;34m"   
#define DARY_GRAY     "\033[1;30m"   
#define CYAN          "\033[0;36m"   
#define LIGHT_CYAN    "\033[1;36m"   
#define PURPLE        "\033[0;35m"   
#define LIGHT_PURPLE  "\033[1;35m"   
#define BROWN         "\033[0;33m"   
#define YELLOW        "\033[1;33m"   
#define LIGHT_GRAY    "\033[0;37m"   
#define WHITE         "\033[1;37m" 
//****************************************************************************************

#define OS_printf(...)  do { printf("[OS] " __VA_ARGS__); fflush(stdout); } while(0)
#define APP_printf(...) do { printf("[APP] " __VA_ARGS__); fflush(stdout); } while(0)
#define ALG_printf(...) do { printf("[ALG] "__VA_ARGS__); fflush(stdout); } while(0)
#define TRACE_printf(...) do { printf(__VA_ARGS__); fflush(stdout); } while(0)


#define OS_ERROR(...) \
			  do \
			  { \
				printf(RED);\
				printf("[APP] ERROR  (%s|%s|%d): ", __FILE__, __func__, __LINE__-1); \
				printf(__VA_ARGS__); fflush(stdout); \
				printf(NONE);\
			  } \
			  while(0);
			  
#define OS_EVENT(...) \
			do \
			{ \
			  printf(GREEN);\
			  printf("[OS] EVENT :" __VA_ARGS__); fflush(stdout); \
			  printf(NONE);\
			} \
			while(0);

#define APP_ALARM(...) \
		  do \
		  { \
			printf(RED);\
			printf("[APP] ALARM :" __VA_ARGS__); fflush(stdout); \
			printf(NONE);\
		  } \
		  while(0);

#define TRACE_ALARM(...) \
		  do \
		  { \
			printf(RED);\
			printf(__VA_ARGS__); fflush(stdout); \
			printf(NONE);\
		  } \
		  while(0);


#define APP_MSG(...) \
		do \
		{ \
		  printf(GREEN);\
		  printf("[APP] MSG :" __VA_ARGS__); fflush(stdout); \
		  printf(NONE);\
		} \
		while(0);

#define APP_EVENT(...) \
		do \
		{ \
		  printf(GREEN);\
		  printf("[APP] EVENT :" __VA_ARGS__); fflush(stdout); \
		  printf(NONE);\
		} \
		while(0);
		
#define TRACE_EVENT(...) \
				do \
				{ \
				  printf(GREEN);\
				  printf(__VA_ARGS__); fflush(stdout); \
				  printf(NONE);\
				} \
				while(0);

#define APP_ERROR(...) \
			  do \
			  { \
				printf(RED);\
				printf("[APP] ERROR  (%s|%s|%d): ", __FILE__, __func__, __LINE__-1); \
				printf(__VA_ARGS__); fflush(stdout); \
				printf(NONE);\
			  } \
			  while(0);

//*****************************************************************************************
#endif